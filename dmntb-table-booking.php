<?php
/**
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link
 * @since			1.0.0
 * @package			dmntb-table-booking
 *
 * 
 * Plugin Name: 	Geekunique Table Booking
 * Plugin URI: 		https://www.geekunique.co.uk/
 * Description: 	Widget to book tables using DesignMyNight Booking APIs
 * Version: 		1.0.0
 * Author: 			geekunique
 * Author URI: 		https://www.geekunique.co.uk/
 * Text Domain: 	dmn-table-booking
 * Domain Path: 	/languages
 * License: 		GNU General Public License v2.0 or later
 * License URI: 	https://www.gnu.org/licenses/gpl-2.0.html
 *
 * Plugin Variable: dmntb
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

define( 'DMNTB_VERSION', '1.0.0' );

/**
 * Begins execution of the plugin.
 * 
 * Return instance of DMNTB.
 *
 * @since   1.0.0
 * @return 	DMNTB
 */
function dmntb()
{
	/**
	 * The core plugin class that is used to define internationalization,
	 * admin-specific hooks, and public-facing site hooks.
	*/
	require_once( 'admin/class-DMNTB.php');

	$plugin = new DMNTB( __FILE__, DMNTB_VERSION );

	return $plugin;
}

dmntb()->init();
