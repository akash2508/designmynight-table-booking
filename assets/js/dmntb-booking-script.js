! function() {
    "use strict";
    window.addEventListener("load", function() {
        var t = document.getElementsByClassName("needs-validation");
        Array.prototype.filter.call(t, function(t) {
            t.addEventListener("submit", function(e) {
                !1 === t.checkValidity() && (e.preventDefault(), e.stopPropagation()), t.classList.add("was-validated")
            }, !1)
        })
    }, !1)
}(), jQuery(function(t) {
    const e = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        n = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        a = new Date,
        i = t => {
            var e = t.toString().split(":"),
                n = 60 * +e[0] + +e[1];
            return parseInt(n)
        },
        o = t => (HHMM = `0${t/60^0}`.slice(-2) + ":" + ("0" + t % 60).slice(-2), HHMM),
        d = t => {
            var e = +t.substr(0, 2),
                n = e < 12 ? "am" : "pm";
            return timeString = (e % 12 || 12) + t.substr(2, 3) + n, timeString
        };
    let r = dmntb_ajax_object.venue_id,
        l = dmntb_ajax_object.source,
        s = dmntb_ajax_object.bearer_token,
        c = "https://api-playpen.designmynight.com/api/v4/venues/" + r + "/booking-availability",
        m = a.getFullYear() + "-" + ("0" + (a.getMonth() + 1)).slice(-2) + "-" + ("0" + a.getDate()).slice(-2),
        u = e[a.getDay()],
        p = i(a.getHours() + ":" + a.getMinutes()),
        f = t(".dmntb_booking_type"),
        v = (t(".dmntb_booking_date"), t(".dmntb_booking_date_elm")),
        b = t(".dmntb_num_people"),
        h = t(".dmntb_arrival_time"),
       // g = (t(".dmntb_leave_time"), t(".dmntbBirthday")),
        g = (t("#saveBookingFormhorizontal .dmntbBirthday")),
        gv = (t("#saveBookingFormvertical .dmntbBirthday")),
        gp = (t("#saveBookingFormprivate-hire .dmntbBirthday")),
        y = t(".dmntb_site_form_submit_button"),
        _ = t(".dmntbModalForm .change-info-close"),
        k = t(".saveBookingForm");
    var D = "",
        x = [],
        w = {
            url: "https://api-playpen.designmynight.com/api/v4/bookings",
            data: {
                action: "create",
                source: l,
                venue_id: r
            },
            dataType: "json",
            type: "POST",
            timeout: 0,
            headers: {
                Authorization: "Bearer " + s
            }
        };
    t.ajax(w).done(function(e) {
        D = e.payload.venueConfig, t.each(e.payload.venueConfig.bookingTypes, function(e, n) {
            x[n.id] = n, f.append(t("<option></option>").attr("value", n.id).text(n.name))
        }), C(f.val(), "onBookingType", f)
    }).fail(function(e, n) {
        console.log(e), t(".dmntb-error").html(e.responseJSON.statusText)
    }), v.tDatePicker({
        autoClose: !0,
        titleCheckIn: "Date of booking",
        dateRangesHover: !1,
        dateRangesShowTitle: !1,
        daysOfWeekHighlighted: [],
        valiDation: !0,
        dateCheckIn: new Date,
        iconDate: '<i class="fas fa-calendar-alt"></i>',
        numCalendar: 1
    }).on("eventClickDay", function() {
        var e = t(this).tDatePicker("getDateInput");
        t(this).next().val(e)
    }), g.datepicker({
        autoclose: !0,
        startDate: new Date((new Date).getFullYear() - 70, 0, 1),
        endDate: new Date((new Date).getFullYear() - 15, 11, 31),
        iconDate: '<i class="fas fa-calendar-alt"></i>',
        //format: 'yyyy-mm-dd',
        orientation: "top auto",
        format: 'yyyy-mm-dd',
        //container: '.modal-content'
        container : 'body'
    }),
    gv.datepicker({
        autoclose: !0,
        startDate: new Date((new Date).getFullYear() - 70, 0, 1),
        endDate: new Date((new Date).getFullYear() - 15, 11, 31),
        iconDate: '<i class="fas fa-calendar-alt"></i>',
        //format: 'yyyy-mm-dd',
        orientation: "top auto",
        format: 'yyyy-mm-dd',
        container : '#dmntbModalFormvertical .modal-content'
        //container: '.modal-content'
    }),
    gp.datepicker({
        autoclose: !0,
        startDate: new Date((new Date).getFullYear() - 70, 0, 1),
        endDate: new Date((new Date).getFullYear() - 15, 11, 31),
        iconDate: '<i class="fas fa-calendar-alt"></i>',
        //format: 'yyyy-mm-dd',
        orientation: "top auto",
        format: 'yyyy-mm-dd',
        container : 'body'
        //container: '.modal-content'
    });
    $(document).mouseup(function(e) 
    {
        if (!g.is(e.target) && g.has(e.target).length === 0) 
        {
            g.closest('.saveBookingForm').css('padding-bottom', '0');
        }
    });
    const C = (e, n, a) => {
        var o = a.closest(".site-booking-form"),
            r = o.find(".dmntb_booking_date"),
            u = o.find(".dmntb_num_people"),
            f = o.find(".dmntb_arrival_time"),
            v = o.find(".dmntb_leave_time");
        if ("onBookingType" != n || f.val()) b = o.find(".dmntb_num_people").val(), h = r.val(), g = f.val(), y = v.val();
        else var b = u.val(),
            h = r.val(),
            g = 0,
            y = 0;
        var _ = {
            url: c,
            data: {
                source: l,
                type: e,
                date: h,
                num_people: b,
                time: g,
                duration: y
            },
            dataType: "json",
            type: "POST",
            timeout: 0,
            headers: {
                Authorization: "Bearer " + s
            }
        };
        t.ajax(_).done(function(e) {
            f.empty();
            var n = 0;
            m === h ? t.each(e.payload.validation.time.suggestedValues, function(e, a) {
                i(a.time) < p && 0 == n ? f.append(t("<option disabled></option>").attr("value", a.time).text(d(a.time))) : (f.append(t("<option></option>").attr("value", a.time).text(d(a.time))), n++)
            }) : t.each(e.payload.validation.time.suggestedValues, function(e, n) {
                f.append(t("<option></option>").attr("value", n.time).text(d(n.time)))
            }), f.trigger("change")
        }).fail(function(t, e) {
            console.log(t), o.find(".dmntb-error").html(t.responseJSON.statusText)
        })
    };
    f.on("change", function() {
        var e = t(this).val();
        C(e, "onBookingType", t(this))
    }), b.on("change", function() {
        t(this).val() >= 8 ? t(this).closest(".site_form_fields").find(".information-field").show() : t(this).closest(".site_form_fields").find(".information-field").hide()
    }), h.on("change", function() {
        var e = t(this).closest(".site-booking-form"),
            n = e.find(".dmntb_booking_type"),
            a = e.find(".dmntb_leave_time");
        a.empty();
        var r = i(t(this).find("option:selected").val()),
            l = i(D.openingTimes[u.toLowerCase()].close);
        n.val(), l < r && (l += 1440);
        for (var s = 15; s <= l - r; s += 15) a.append(t("<option></option>").attr("value", s).text(d(o((r + s) % 1440))))
    }), y.on("click", function() {
        var a = t(this).closest(".site-booking-form"),
            i = a.find(".dmntb_booking_type"),
            o = a.find(".dmntb_booking_date"),
            r = a.find(".dmntb_booking_date_elm"),
            l = a.find(".dmntb_num_people"),
            s = a.find(".dmntb_arrival_time"),
            c = a.find(".dmntb_leave_time"),
            u = t(this).data("modal-id"),
            p = t(this).data("style"),
            f = t("#" + u);
        if ("vertical" != p) {
            var v = t.fn.modal.Constructor.prototype._enforceFocus;
            t.fn.modal.Constructor.prototype._enforceFocus = function() {}, f.on("hidden", function() {
                t.fn.modal.Constructor.prototype._enforceFocus = v
            }), f.appendTo("body").modal("show")
        } else a.hide("", function() {
            t(this).attr("style", "display: none !important")
        }), f.show();
        var b = i.val(),
            h = o.val(),
            g = r.tDatePicker("getDate"),
            y = l.val(),
            _ = s.val(),
            k = c.val();
        m === h ? f.find(".notice").show() : f.find(".notice").hide(), f.find('input[name="dmntb_booking_type"]').val(b), f.find('input[name="dmntb_booking_date"]').val(h), f.find('input[name="dmntb_num_people"]').val(y), f.find('input[name="dmntb_arrival_time"]').val(_), f.find('input[name="dmntb_leave_time"]').val(k);
        var D = f.find(".modal-form-title");
        D.find(".booking_type").text(i.find("option:selected").text()), D.find(".num_people").text(y), D.find(".arrival_time").text(d(_)), D.find(".booking_date").text((t => {
            var a = "",
                i = ("0" + t.getDate()).slice(-2);
            switch (i) {
                case "01":
                case "21":
                case "31":
                    a = "st";
                    break;
                case "02":
                case "22":
                    a = "nd";
                    break;
                case "03":
                case "23":
                    a = "rd";
                    break;
                default:
                    a = "th"
            }
            return e[t.getDay()] + ", " + i + a + " " + n[t.getMonth()] + " " + t.getFullYear()
        })(g))
    }), _.on("click", function() {
        var e = t(this).closest(".dmntbModalForm"),
            n = e.prev();
        e.hide(), n.show("", function() {
            t(this).attr("style", "display: flex")
        })
    }), k.on("submit", function(e) {
        var n, a, i, o;
        if (e.preventDefault(), document.getElementById("saveBookingForm"), !0 === this.checkValidity()) return n = t(this), a = n.closest(".dmntbModalForm"), i = {
            action: "dmntb_submit_booking_data",
            data: n.serialize()
        }, o = {
            url: dmntb_ajax_object.ajaxurl,
            data: i,
            dataType: "json",
            type: "POST",
            timeout: 0
        }, t.ajax(o).done(function(t) {
            t.flag ? alert(t.message) : a.find(".modal-body").html(t.message)
        }).fail(function(t, e) {
            console.log(e)
        }), !0
    })
});