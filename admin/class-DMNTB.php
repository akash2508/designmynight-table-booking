<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link
 * @since      1.0.0
 *
 * @package    dmntb-table-booking
 * @subpackage dmntb-table-booking/admin
 */
class DMNTB
{

	/**
	 * Filepath of main plugin file.
	 *
	 * @var string
	 */
	public $file;

	/**
	 * Plugin version.
	 *
	 * @var string
	 */
	public $version;

	/**
	 * Absolute plugin path.
	 *
	 * @var string
	 */
	public $plugin_path;

	/**
	 * Absolute plugin URL.
	 *
	 * @var string
	 */
	public $plugin_url;

	/**
	 * Constructor.
	 *
	 * @param string $file    Filepath of main plugin file
	 * @param string $version Plugin version
	 */
	public function __construct( $file, $version )
	{
		$this->file    = $file;
		$this->version = $version;

		// Path.
		$this->plugin_path   = trailingslashit( plugin_dir_path( $this->file ) );
		$this->plugin_url    = trailingslashit( plugin_dir_url( $this->file ) );
	}

	public function init()
	{
		register_activation_hook( $this->file, array( $this, 'dmntb_plugin_activation' ) );
		register_deactivation_hook( __FILE__, array( $this, 'dmntb_plugin_deactivation' ) );
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
		add_filter( 'plugin_action_links_'.plugin_basename( $this->file ), array( $this, 'plugin_action_links' ) );
		add_action( 'admin_menu', array( $this, 'dmntb_plugin_menu' ) );

		add_action( 'wp_enqueue_scripts', array( $this, 'dmntb_front_enqueue' ) );
		add_shortcode( 'dmntb_booking', array( $this, 'dmntb_booking_shortcode' ) );
		add_action( 'wp_ajax_dmntb_submit_booking_data', array( $this, 'dmntb_submit_booking_data') );
		add_action( 'wp_ajax_nopriv_dmntb_submit_booking_data', array( $this, 'dmntb_submit_booking_data') );
	}

	/**
	 * The code that runs during plugin activation.
	 */
	public function dmntb_plugin_activation()
	{

	}

	/**
	 * The code that runs during plugin deactivation.
	 */
	public function dmntb_plugin_deactivation()
	{

	}

	/**
	 * Load localisation files.
	 *
	 * @since 1.0.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'dmntb-table-booking', false, plugin_basename( $this->plugin_path ) . '/languages' );
	}

	/**
	 * Add relevant links to plugins page.
	 *
	 * @since 1.0.0
	 *
	 * @param array $links Plugin action links
	 *
	 * @return array Plugin action links
	 */
	public function plugin_action_links( $links )
	{
		$plugin_links = array();
		$plugin_links[] = '<a href="' .admin_url( 'options-general.php?page=dmntb-setting' ) .'">' . esc_html__('Settings') . '</a>';
		$plugin_links[] = '<a href="' .admin_url( 'options-general.php?page=dmntb-setting&usage=1' ) .'">' . esc_html__('How to use?') . '</a>';
		return array_merge( $plugin_links, $links );
	}

	public function dmntb_plugin_menu()
	{
		add_submenu_page( 'options-general.php', __('DMNTB Form Settings' ), __('DMNTB Form Settings' ), 'manage_options', 'dmntb-setting', array( $this, 'dmntb_settings' ));
	}

	/**
	 * Enqueue scripts and styles.
	 */
	public function dmntb_front_enqueue()
	{

		$dmntb_venue_id = get_option('dmntb_venue_id') ? get_option('dmntb_venue_id') : '';
		$dmntb_source = get_option('dmntb_source') ? get_option('dmntb_source') : 'partner';
		$dmntb_bearer_token = get_option('dmntb_bearer_token') ? get_option('dmntb_bearer_token') : '';

		$params = array( 
			'ajaxurl' => admin_url('admin-ajax.php'), 
			'venue_id' => sanitize_text_field($dmntb_venue_id), 
			'source' => sanitize_text_field($dmntb_source), 
			'bearer_token' => sanitize_text_field($dmntb_bearer_token),
		);

		//CSS
		wp_register_style('t-datepicker-css', $this->plugin_url.'assets/css/t-datepicker.min.css');
		wp_register_style('t-datepicker-theme-css', $this->plugin_url.'assets/css/themes/t-datepicker-bluegrey.css');
		// wp_register_style( 'jquery-ui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css', array(), '1.12.1', 'all');
		wp_register_style( 'bootstrap-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array(), '4.1.3', 'all');
		wp_register_style( 'bootstrap-datepicker', $this->plugin_url.'assets/css/bootstrap-datepicker.css', array(), '4.1.3', 'all');
		// wp_register_style( 'dmntb-booking-style', $this->plugin_url.'/assets/css/dmntb-booking-style.css' );

		//JS
		wp_register_script('t-datepicker-js', $this->plugin_url.'assets/js/t-datepicker.min.js', array('jquery'), '1.0.1', true);
		//wp_register_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array('jquery'), '4.1.3', true );

		wp_register_script( 'bootstrap-datepicker-js', $this->plugin_url.'assets/js/bootstrap-datepicker.js', array('jquery'), '4.1.3', true );
		wp_register_script( 'bootstrap-datepicker-en-js', $this->plugin_url.'assets/js/bootstrap-datepicker.en-IE.min.js', array('jquery'), '1.0.0', true );
		wp_register_script( 'dmntb-booking-script', $this->plugin_url.'assets/js/dmntb-booking-script.js', array('jquery'), $this->version, true );
		wp_localize_script( 'dmntb-booking-script', 'dmntb_ajax_object', $params );
	}

	/**
	 * Function used to show the settings page
	 */
	public function dmntb_settings()
	{
		require_once $this->plugin_path . '/admin/dmntb_settings_page.php'; 
	}

	/**
	 * Init Booking shortcode.
	 * 
	 * @param  array  	$atts
	 * @param  string 	$content
	 * @param  string 	$tag
	 * 
	 * @return string 	HTML Content
	 */
	public function dmntb_booking_shortcode($atts = [], $content = null, $tag = '')
	{
		global $wp, $wpdb;

		// normalize attribute keys, lowercase
		$atts = array_change_key_case((array)$atts, CASE_LOWER);

		// override default attributes with user attributes
		$dmntb_atts = shortcode_atts([
			'style' => 'horizontal',
		], $atts, $tag);

		$style = $dmntb_atts['style'];
		$showFields = ($style=='horizontal') ? 'style="display: none;"' : '';

		wp_enqueue_style( 't-datepicker-css' );
		wp_enqueue_style( 't-datepicker-theme-css' );
		wp_enqueue_style( 'bootstrap-datepicker' );
		wp_enqueue_style( 'dmntb-booking-style' );

		wp_enqueue_script( 'bootstrap-datepicker-js' );
		wp_enqueue_script( 't-datepicker-js' );
		wp_enqueue_script( 'dmntb-booking-script' );

		$num_people = '';
		for ($i=1; $i <= 150; $i++) { 
			$num_people .= '<option value="'.$i.'">'.$i.'</option>';
		}

		$dmntb_booking_form_nonce = wp_create_nonce( 'dmntb_booking_form_verify' );

		//Get the default or updated meta values
		$dmntb_booking_type = get_option('dmntb_booking_type') ? get_option('dmntb_booking_type') : 'Type of booking';
		$dmntb_booking_date = get_option('dmntb_booking_date') ? get_option('dmntb_booking_date') : 'Date of booking';
		$dmntb_num_people = get_option('dmntb_num_people') ? get_option('dmntb_num_people') : 'Number of guests';
		$dmntb_arrival_time = get_option('dmntb_arrival_time') ? get_option('dmntb_arrival_time') : 'Arrival Time';
		$dmntb_leave_time = get_option('dmntb_leave_time') ? get_option('dmntb_leave_time') : 'Leave time';
		$dmntb_book_a_table = get_option('dmntb_book_a_table') ? get_option('dmntb_book_a_table') : 'Book a table';

		$dmntb_book_a_table = ($style=='horizontal') ? $dmntb_book_a_table : 'Continue';

		$html = '<form action="'.home_url( $wp->request ).'" method="post" class="book-table-form-coll site-booking-form d-flex flex-wrap form-'.$style.'" class="needs-validation" novalidate>
			<div class="site_form_body">
				<ul class="site_form_fields d-flex flex-wrap">
					<li class="site_form_field book-filed-one">
						<label class="site_form_label" for="dmntb_booking_type'.$style.'">'.$dmntb_booking_type.'</label>
						<div class="site_form_input_container">
							<select name="dmntb_booking_type" id="dmntb_booking_type'.$style.'" class="dmntb_booking_type" required>
							</select>
						</div>
					</li>
					<li class="site_form_field book-filed-two" '.$showFields.'>
						<div class="t-datepicker dmntb_booking_date_elm">
							<label class="site_form_label" for="t-check-in" style="width: 100%;">'.$dmntb_booking_date.'
								<div class="t-check-in t-picker-only"></div>
							</label>
						</div>
						<input type="hidden" name="dmntb_booking_date" class="dmntb_booking_date" value="'.date('Y-m-d').'">
					</li>
					<li class="site_form_field book-filed-three " '.$showFields.'>
						<label class="site_form_label" for="dmntb_num_people">'.$dmntb_num_people.'</label>
						'.(($style!='vertical') ? '<span class="information-field" style="display:none">
							<div class="info-det">
							 	<div class="info-img">
							 		<img src="'. $this->plugin_url .'assets/images/Information.svg" alt="information-icon">
							 	</div>
								<div class="info-hide">You have selected 8 or more guests. This booking will change to an enquiry. We will contact you regarding your requirements.</div>
							</div>
						</span>' : '' ).'
						<div class="site_form_input_container">
							<select name="dmntb_num_people" class="dmntb_num_people" required>'.$num_people.'</select>
						</div>
					</li>
					'.(($style=='vertical') ? '<span class="information-field number-guest" style="display:none"><img src="'. $this->plugin_url .'/assets/images/Information.svg" alt="information-icon" class="mCS_img_loaded"> You have selected 8 or more guests. This booking will change to an enquiry. We will contact you regarding your requirements.</span>' : '' ).'
					<li class="site_form_field book-filed-four" '.$showFields.'>
						<label class="site_form_label" for="dmntb_arrival_time">'.$dmntb_arrival_time.'</label>
						<div class="site_form_input_container">
							<select name="dmntb_arrival_time" class="dmntb_arrival_time" required></select>
						</div>
					</li>
					<li class="site_form_field book-filed-five" '.$showFields.'>
						<label class="site_form_label" for="dmntb_leave_time">'.$dmntb_leave_time.'</label>
						<div class="site_form_input_container">
							<select name="dmntb_leave_time" class="dmntb_leave_time" required></select>
						</div>
					</li>
					<li class="site_form_footer-btn" '.$showFields.'>
						<div class="site_form_footer">
							<input type="button" class="dmntb_site_form_submit_button site_form_button dmntb_book_a_table" value="'.$dmntb_book_a_table.'" data-modal-id="dmntbModalForm'.$style.'" data-style="'.$style.'">
						</div>
					</li>
				</ul><div class="dmntb-error"></div>
				'.( ($style=='horizontal') ? '<div class="slide-down-form slide-btn-'.$style.'" id="slide-down-form" style="cursor: pointer;">
					<i class="fas fa-chevron-down"></i>
				</div>' : '' ).'
			</div>
		</form>';

		$submittionForm = '<h4 class="modal-form-title">You are making a <span class="booking_type"></span> booking for <span class="num_people"></span> people at <span class="arrival_time"></span> on <span class="booking_date">11 Mar 2020</span>.</h4>
						'.( ($style=='horizontal') ? '<button type="button" class="close change-info-close" data-dismiss="modal" aria-label="Close">Change</button>' : '<button type="button" class="close change-info-close">Change</button>' ).'
						<form action="'.home_url( $wp->request ).'/save_booking_data" method="post" id="saveBookingForm'.$style.'" class="needs-validation mb-1 saveBookingForm" novalidate>
							<input type="hidden" name="dmntb_booking_form_verify" value="'.$dmntb_booking_form_nonce.'">
							<input type="hidden" name="dmntb_booking_type">
							<input type="hidden" name="dmntb_booking_date">
							<input type="hidden" name="dmntb_num_people">
							<input type="hidden" name="dmntb_arrival_time">
							<input type="hidden" name="dmntb_leave_time">
							<div class="alert alert-warning notice"><i class="fa fa-clock"></i> Note: We highly recommend booking at least 24 hours prior to your preferred date.</div>
							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="dmntbEmail'.$style.'" class="col-form-label">Email <span class="required">*</span></label>
									<input type="email" class="form-control" name="email" id="dmntbEmail'.$style.'" placeholder="Email" required>
									<small class="form-text text-muted">We\'ll never share your email with anyone else.</small>
									<div class="invalid-feedback"><i class="fa fa-exclamation-circle"></i> Please enter your valid email address.</div>
								</div>
								<div class="form-group col-md-6">
									<label for="dmntbFName'.$style.'" class="col-form-label">First Name <span class="required">*</span></label>
									<input type="text" class="form-control" name="fname" id="dmntbFName'.$style.'" placeholder="First Name" required>
									<div class="invalid-feedback"><i class="fa fa-exclamation-circle"></i> Please enter your first name.</div>
								</div>
								<div class="form-group col-md-6">
									<label for="dmntbLName'.$style.'" class="col-form-label">Last Name <span class="required">*</span></label>
									<input type="text" class="form-control" name="lname" id="dmntbLName'.$style.'" placeholder="Last Name" required>
									<div class="invalid-feedback"><i class="fa fa-exclamation-circle"></i> Please enter your last name.</div>
								</div>
								<div class="form-group col-md-6">
									<label for="dmntbMobileNo'.$style.'" class="col-form-label">Mobile Number <span class="required">*</span> <i class="fa fa-question-circle text-info" title="Your mobile number is used for free texts acknowledging and confirming your booking."></i></label>
									<input type="tel" class="form-control" name="mobile" id="dmntbMobileNo'.$style.'" placeholder="Mobile Number" required>
									<div class="invalid-feedback"><i class="fa fa-exclamation-circle"></i> Please enter your phone number.</div>
								</div>
								<div class="form-group col-md-6">
									<label for="dmntbBirthday'.$style.'" class="col-form-label">Birthday</label>
									
									<input type="text" class="form-control dmntbBirthday" name="birthday" placeholder="Birthday">
									<div class="invalid-feedback"></div>
								</div>
								<div class="form-group col-md-12">
									<label for="dmntbSpecialRequest'.$style.'" class="col-form-label">Special Requests</label>
									<textarea placeholder="Special Requests" class="form-control" name="notes" id="dmntbSpecialRequest'.$style.'"></textarea>
								</div>
							</div>
							<div class="alert alert-warning alert-dismissible fade show" role="alert">
								<div class="bookingPolicy mb-3"><small><span class="ng-binding"><i class="fa fa-exclamation-circle"></i> Booking Policy:</span> Please note that our window tables are first come first served on the day. Bookings over 8 will require a pre-order</small></div>
								<div class="form-check custom-control custom-checkbox">
									<input class="form-check-input custom-control-input" type="checkbox" value="" id="invalidCheck'.$style.'" required>
									<label class="form-check-label custom-control-label" for="invalidCheck'.$style.'">
										I confirm I have read this booking policy.
									</label>
									<div class="invalid-feedback">
										You must agree booking policy before submitting.
									</div>
								</div>
							</div>
							<button class="site-btn d-block" type="submit">Book Now</button>
						</form>';

		$html .= ($style!='vertical') ? '<div class="modal fade dmntbModalForm" id="dmntbModalForm'.$style.'" tabindex="-1" role="dialog" aria-labelledby="dmntbModalForm" aria-hidden="false" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						'. $submittionForm .'
					</div>
				</div>
			</div>
		</div>' : '<div class="dmntbModalForm" id="dmntbModalForm'.$style.'" style="display:none;">
			<div class="modal-content">
				<div class="modal-body">
					'. $submittionForm .'
				</div>
			</div>
		</div>';

		return $html;
	}

	/**
	 * Init Ajax on booking submission.
	 */
	public function dmntb_submit_booking_data()
	{	
		$params = array();
		$response = $message = '';
		$flag = 0;
		$dmntb_venue_id = get_option('dmntb_venue_id') ? get_option('dmntb_venue_id') : '';
		$dmntb_source = get_option('dmntb_source') ? get_option('dmntb_source') : 'partner';
		$dmntb_bearer_token = get_option('dmntb_bearer_token') ? get_option('dmntb_bearer_token') : '';
		$dmntb_success_message = get_option('dmntb_success_message') ? get_option('dmntb_success_message') : '';

		//Email content from admin
		$dmntb_email_subject = get_option('dmntb_email_subject') ? get_option('dmntb_email_subject') : '';
		$dmntb_email_message = get_option('dmntb_email_message') ? get_option('dmntb_email_message') : '';

		//Get form serialize data
		$data = $_POST['data'];

		//Parse all form data
		parse_str($data, $params);

		// Check that nonce field
		$nonce = wp_verify_nonce( $params['dmntb_booking_form_verify'], 'dmntb_booking_form_verify' );
		if ( !$nonce ) {
			$message =  'Nonce is invalid';
			$flag = 1;
		} else {
			if($dmntb_bearer_token) {
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL => 'https://api-playpen.designmynight.com/api/v4/bookings?source='.sanitize_text_field($dmntb_source).'&venue_id='.sanitize_text_field($dmntb_venue_id).'&first_name='.sanitize_text_field($params['fname']).'&last_name='.sanitize_text_field($params['lname']).'&num_people='.sanitize_text_field($params['dmntb_num_people']).'&type='.sanitize_text_field($params['dmntb_booking_type']).'&date='.sanitize_text_field($params['dmntb_booking_date']).'&time='.sanitize_text_field($params['dmntb_arrival_time']).'&email='.sanitize_text_field($params['email']).'&duration='.sanitize_text_field($params['dmntb_leave_time']).'&phone='.sanitize_text_field($params['mobile']).'&notes='.urlencode(sanitize_text_field($params['notes'])).'&dob='.sanitize_text_field($params['birthday']),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_HTTPHEADER => array(
						"Authorization: Bearer ".$dmntb_bearer_token
					),
				));

				$response = json_decode(curl_exec($curl));
				if($response == null) {
					$response = 'https://api-playpen.designmynight.com/api/v4/bookings?source='.sanitize_text_field($dmntb_source).'&venue_id='.sanitize_text_field($dmntb_venue_id).'&first_name='.sanitize_text_field($params['fname']).'&last_name='.sanitize_text_field($params['lname']).'&num_people='.sanitize_text_field($params['dmntb_num_people']).'&type='.sanitize_text_field($params['dmntb_booking_type']).'&date='.sanitize_text_field($params['dmntb_booking_date']).'&time='.sanitize_text_field($params['dmntb_arrival_time']).'&email='.sanitize_text_field($params['email']).'&duration='.sanitize_text_field($params['dmntb_leave_time']).'&phone='.sanitize_text_field($params['mobile']).'&notes='.urlencode(sanitize_text_field($params['notes'])).'&dob='.sanitize_text_field($params['birthday']);
				}
				curl_close($curl);

				if($response && $response->payload->bookingStatus == 'received') {
					$params['venue_name'] = $response->payload->venue->title;
					$params['booking_id'] = $response->payload->booking->booking_id;
					$params['refence_code'] = $response->payload->booking->reference;
					$params['booking_type'] = $response->payload->booking->type->name;
					$message = self::dmntb_mailvarstr(stripslashes($dmntb_success_message), $params);
					$flag = 0;

					//Send email after successfull booking
					$from = null;
					$to =sanitize_text_field($params['email']);
					$subject = self::dmntb_mailvarstr(stripslashes($dmntb_email_subject), $params);
					$content = self::dmntb_mailvarstr(stripslashes($dmntb_email_message), $params);
					self::dmntbHtmlEmail($from, $to, $subject, $content);
				} else {
					$message = 'There are some issue in submitting booking. Please try again!';
					$flag = 1;
				}
			} else {
				$message = 'Bearer token is unauthosized or not added!';
				$flag = 1;
			}
		}

		echo wp_send_json(array( 'response' => $response, 'message' => $message, 'flag' => $flag ));
		wp_die();
	}

	/**
	 * Function used for parsed the variable with the booking values in given string
	 * 
	 * @param  string 	$string
	 * @param  array 	$data
	 * 
	 * @return string   parsed data
	 */
	public function dmntb_mailvarstr($string, $data)
	{
	    $vars = array(
	      '{venue_name}'		=> $data['venue_name'],
	      '{first_name}'		=> $data['fname'],
	      '{last_name}'			=> $data['lname'],
	      '{num_of_people}'		=> $data['dmntb_num_people'],
	      '{booking_date}'		=> $data['dmntb_booking_date'],
	      '{arrival_time}'		=> $data['dmntb_arrival_time'],
	      '{booking_id}'		=> $data['booking_id'],
	      '{refence_code}'		=> $data['refence_code'],
	      '{booking_type}'		=> $data['booking_type'],
	      '{special_request}'	=> $data['notes'],
	    );

	    return strtr($string, $vars);
	}

	/**
	 * Function to send email using WordPress wp_email
	 * 
	 * @param  string 	$from
	 * @param  string 	$to 
	 * @param  string 	$subject
	 * @param  string 	$message
	 * @param  string 	$headers
	 * 
	 * @return boolean  Email status
	 */
	public function dmntbHtmlEmail($from, $to, $subject, $message, $headers = "")
	{
	    if( is_null($from) ) {
	        $from = get_option( 'admin_email' );
	    }
	    $headers = "From: " . strip_tags($from) . "\r\n";
	    $headers .= "Reply-To: ". strip_tags($from) . "\r\n";
	    $headers .= "MIME-Version: 1.0\r\n";
	    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	    
	    $status = wp_mail($to, $subject, $message, $headers);

	    return $status;
	}
}