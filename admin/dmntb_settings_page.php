<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wpdb;
if ( !current_user_can( 'manage_options' ) )
{
	wp_die( 'You are not allowed to be on this page.' );
}

$usage = isset($_GET['usage']) ? $_GET['usage'] : '';

if ( isset($_POST['dmntb_settings_verify']) ) {
	// Check that nonce field
	wp_verify_nonce( $_POST['dmntb_settings_verify'], 'dmntb_settings_verify' );

	//Get form values
	$dmntb_venue_id_field		= sanitize_text_field( $_POST['dmntb_venue_id'] );
	$dmntb_source_field			= sanitize_text_field( $_POST['dmntb_source'] );
	$dmntb_bearer_token_field	= sanitize_text_field( $_POST['dmntb_bearer_token'] );
	$dmntb_booking_type_field	= sanitize_text_field( $_POST['dmntb_booking_type'] );
	$dmntb_booking_date_field	= sanitize_text_field( $_POST['dmntb_booking_date'] );
	$dmntb_num_people_field		= sanitize_text_field( $_POST['dmntb_num_people'] );
	$dmntb_arrival_time_field	= sanitize_text_field( $_POST['dmntb_arrival_time'] );
	$dmntb_leave_time_field		= sanitize_text_field( $_POST['dmntb_leave_time'] );
	$dmntb_book_a_table_field	= sanitize_text_field( $_POST['dmntb_book_a_table'] );
	$dmntb_sender_name_field	= sanitize_text_field( $_POST['dmntb_sender_name'] );
	$dmntb_sender_email_field	= sanitize_email( $_POST['dmntb_sender_email'] );
	$dmntb_success_message_field =  $_POST['dmntb_success_message'];
	$dmntb_email_subject_field =  $_POST['dmntb_email_subject'];
	$dmntb_email_message_field =  $_POST['dmntb_email_message'];

	//Update form values to wp_options meta
	update_option('dmntb_venue_id', $dmntb_venue_id_field);
	update_option('dmntb_source', $dmntb_source_field);
	update_option('dmntb_bearer_token', $dmntb_bearer_token_field);
	update_option('dmntb_booking_type', $dmntb_booking_type_field);
	update_option('dmntb_booking_date', $dmntb_booking_date_field);
	update_option('dmntb_num_people', $dmntb_num_people_field);
	update_option('dmntb_arrival_time', $dmntb_arrival_time_field);
	update_option('dmntb_leave_time', $dmntb_leave_time_field);
	update_option('dmntb_book_a_table', $dmntb_book_a_table_field);
	update_option('dmntb_success_message', $dmntb_success_message_field);
	update_option('dmntb_sender_name', $dmntb_sender_name_field);
	update_option('dmntb_sender_email', $dmntb_sender_email_field);
	update_option('dmntb_email_subject', $dmntb_email_subject_field);
	update_option('dmntb_email_message', $dmntb_email_message_field);

	$settype = 'updated';
	$setmessage = __('Your Settings Saved Successfully.');
	add_settings_error(
		'dmntb_settings_updated',
		esc_attr( 'settings_updated' ),
		$setmessage,
		$settype
	);
}

//Get the default or updated meta values
$dmntb_bearer_token = get_option('dmntb_bearer_token') ? get_option('dmntb_bearer_token') : '';
$dmntb_venue_id = get_option('dmntb_venue_id') ? get_option('dmntb_venue_id') : '';
$dmntb_source = get_option('dmntb_source') ? get_option('dmntb_source') : 'partner';
$dmntb_booking_type = get_option('dmntb_booking_type') ? get_option('dmntb_booking_type') : 'Type of booking';
$dmntb_booking_date = get_option('dmntb_booking_date') ? get_option('dmntb_booking_date') : 'Date of booking';
$dmntb_num_people = get_option('dmntb_num_people') ? get_option('dmntb_num_people') : 'Number of guests';
$dmntb_arrival_time = get_option('dmntb_arrival_time') ? get_option('dmntb_arrival_time') : 'Arrival Time';
$dmntb_leave_time = get_option('dmntb_leave_time') ? get_option('dmntb_leave_time') : 'Leave time';
$dmntb_book_a_table = get_option('dmntb_book_a_table') ? get_option('dmntb_book_a_table') : 'Book a table';
$dmntb_success_message = get_option('dmntb_success_message') ? get_option('dmntb_success_message') : '';
$dmntb_sender_name = get_option('dmntb_sender_name') ? get_option('dmntb_sender_name') : get_bloginfo( 'name' );
$dmntb_sender_email = get_option('dmntb_sender_email') ? get_option('dmntb_sender_email') : get_option('admin_email');
$dmntb_email_subject = get_option('dmntb_email_subject') ? get_option('dmntb_email_subject') : '';
$dmntb_email_message = get_option('dmntb_email_message') ? get_option('dmntb_email_message') : '';
?>
<div class="wrap dmntb-setting-page">
	<h1><?php echo __( 'DMNTB Form Settings' ) ?></h1><br>
	<?php settings_errors(); ?>
	<div class="nav-tab-wrapper">
		<a class="nav-tab <?php if(!$usage): ?>nav-tab-active<?php endif; ?>" href="?page=dmntb-setting"><?php echo __( 'General Settings', 'dmn-table-booking' ) ?></a>
		<a class="nav-tab <?php if($usage): ?>nav-tab-active<?php endif; ?>" href="?page=dmntb-setting&usage=1"><?php echo __( 'How to use?', 'dmn-table-booking' ) ?></a>
	</div>
	<div class="content">
		<?php if(!$usage): ?>
			<form method="post" name="dmntb-settings" id="dmntb-settings" action="<?php echo admin_url( 'options-general.php' ); ?>?page=dmntb-setting&settings=1">
				<?php $dmntbnonce = wp_create_nonce( 'dmntb_settings_verify' ); ?>
				<input type="hidden" name="dmntb_settings_verify" value="<?php echo($dmntbnonce); ?>">
				<table class="form-table" id="general">
					<tbody>
						<tr>
							<th scope="row">
								<label for="dmntb_venue_id"><?php echo __( 'Venue ID' ); ?></label>
							</th>
							<td>
								<input type="text" name="dmntb_venue_id" id="dmntb_venue_id" value="<?php echo $dmntb_venue_id; ?>" class="regular-text" aria-required="true" onfocus="this.select();" >
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_source"><?php echo __( 'Booking Source' ); ?></label>
							</th>
							<td>
								<input type="text" name="dmntb_source" id="dmntb_source" value="<?php echo $dmntb_source; ?>" class="regular-text" aria-required="true" onfocus="this.select();" readonly>
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_bearer_token"><?php echo __( 'Bearer Token' ); ?></label>
							</th>
							<td>
								<textarea type="text" name="dmntb_bearer_token" id="dmntb_bearer_token" class="regular-text" aria-required="true" onfocus="this.select();" rows="8"><?php echo $dmntb_bearer_token; ?></textarea>
							</td>
						</tr>
						<tr>
							<th colspan="2" style="padding:0">
								<h3><?php echo __( 'Form Shortcodes' ); ?></h3>
							</th>
						</tr>
						<tr>
							<th scope="row">
								<label for="horizontal_form"><?php echo __( 'Horizontal Form Shortcode' ); ?></label>
							</th>
							<td>
								<input type="text" id="horizontal_form" value='[dmntb_booking style="horizontal"]' class="regular-text" aria-required="true" readonly="readonly" onfocus="this.select();" >
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="vertical_form"><?php echo __( 'Vertical Form Shortcode' ); ?></label>
							</th>
							<td>
								<input type="text" id="vertical_form" value='[dmntb_booking style="vertical"]' class="regular-text" aria-required="true" readonly="readonly" onfocus="this.select();" >
							</td>
						</tr>
						<tr><th colspan="2"><hr></th></tr>
						<tr>
							<th colspan="2" style="padding:0">
								<h3><?php echo __( 'Form Label Settings' ); ?></h3>
							</th>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_booking_type"><?php echo __( 'Type of booking' ); ?></label>
							</th>
							<td>
								<input type="text" name="dmntb_booking_type" id="dmntb_booking_type" value="<?php echo $dmntb_booking_type; ?>" class="regular-text" aria-required="true" >
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_booking_date"><?php echo __( 'Date of booking' ); ?></label>
							</th>
							<td>
								<input type="text" name="dmntb_booking_date" id="dmntb_booking_date" value="<?php echo $dmntb_booking_date; ?>" class="regular-text" aria-required="true" >
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_num_people"><?php echo __( 'Number of guests' ); ?></label>
							</th>
							<td>
								<input type="text" name="dmntb_num_people" id="dmntb_num_people" value="<?php echo $dmntb_num_people; ?>" class="regular-text" aria-required="true" >
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_arrival_time"><?php echo __( 'Arrival Time' ); ?></label>
							</th>
							<td>
								<input type="text" name="dmntb_arrival_time" id="dmntb_arrival_time" value="<?php echo $dmntb_arrival_time; ?>" class="regular-text" aria-required="true" >
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_leave_time"><?php echo __( 'Leave time' ); ?></label>
							</th>
							<td>
								<input type="text" name="dmntb_leave_time" id="dmntb_leave_time" value="<?php echo $dmntb_leave_time; ?>" class="regular-text" aria-required="true" >
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_book_a_table"><?php echo __( 'Book a table' ); ?></label>
							</th>
							<td>
								<input type="text" name="dmntb_book_a_table" id="dmntb_book_a_table" value="<?php echo $dmntb_book_a_table; ?>" class="regular-text" aria-required="true" >
							</td>
						</tr>
						<tr><th colspan="2"><hr></th></tr>
						<tr>
							<th scope="row">
								<label for="dmntb_success_message"><?php echo __( 'Success Message', 'dmn-table-booking' ); ?></label>
								<p class="description"><?php echo __( 'This message will show on popup after successful booking.', 'dmn-table-booking' ); ?></p>
							</th>
							<td>
								<?php wp_editor( stripslashes($dmntb_success_message), 'dmntb_success_message', array('wpautop' => false, 'media_buttons' => false, 'textarea_rows' => 5 ) ); ?>
								<p class="description">{venue_name} {first_name} {last_name} {num_of_people} {booking_date} {arrival_time} {refence_code} {booking_type} {special_request} {booking_id}</p>
							</td>
						</tr>
						<tr><th colspan="2"><hr></th></tr>
						<tr>
							<th colspan="2" style="padding:0">
								<h3><?php echo __( 'Email Settings' ); ?></h3>
							</th>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_sender_name"><?php echo __( 'Sender Name', 'dmn-table-booking' ); ?></label>
								<p class="description"><?php echo __( 'For emails send by this plugin.', 'dmn-table-booking' ); ?></p>
							</th>
							<td>
								<input name="dmntb_sender_name" type="text" id="dmntb_sender_name" value="<?php echo esc_html( $dmntb_sender_name ); ?>" class="regular-text" aria-describedby="sendername-description">
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_sender_email"><?php echo __( 'Sender Email', 'dmn-table-booking' ); ?></label>
								<p class="description"><?php echo __( 'For emails send by this plugin.', 'dmn-table-booking' ); ?></p>
							</th>
							<td>
								<input name="dmntb_sender_email" type="email" id="dmntb_sender_email" value="<?php echo esc_html( $dmntb_sender_email ); ?>" class="regular-text" aria-describedby="senderemail-description">
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_email_subject"><?php echo __( 'Email Subject', 'dmn-table-booking' ); ?></label>
								<p class="description"><?php echo __( 'Subject for emails send to customers after confirm booking enquiry.', 'dmn-table-booking' ); ?></p>
							</th>
							<td>
								<input type="text" name="dmntb_email_subject" id="dmntb_email_subject" value="<?php echo stripslashes($dmntb_email_subject); ?>" class="regular-text" aria-required="true" style="width: 100%;">
								<p class="description">{venue_name} {first_name} {last_name} {num_of_people} {booking_date} {arrival_time} {refence_code} {booking_type} {special_request} {booking_id}</p>
							</td>
						</tr>
						<tr>
							<th scope="row">
								<label for="dmntb_email_message"><?php echo __( 'Email Body', 'dmn-table-booking' ); ?></label>
								<p class="description"><?php echo __( 'Body message for emails send to customers after confirm booking enquiry.', 'dmn-table-booking' ); ?></p>
							</th>
							<td>
								<?php wp_editor( stripslashes($dmntb_email_message), 'dmntb_email_message', array('wpautop' => false, 'media_buttons' => false, 'textarea_rows' => 5 ) ); ?>
								<p class="description">{venue_name} {first_name} {last_name} {num_of_people} {booking_date} {arrival_time} {refence_code} {booking_type} {special_request} {booking_id}</p>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit">
					<?php submit_button( __( 'Save Settings' ), 'primary', 'submit', false ); ?>
				</p>
			</form>
			<?php else: ?>
				<h3><?php echo __( 'General Settings' ); ?></h3>
				<p>Add <code>Venue ID</code> from <a href="https://admin-playpen.designmynight.com/collins/venue-rules" target="_blank">designmynight admin panel</a>. Also <code>Bearer Token</code> for authorization of Booking APIs.</p>
				<p>There are other settings for form fields. You can change the form field names and add your custom thank you message after confirm the booking enquiry.</p>
				<p>You can also customize email subject and body content, which email we are sending to customers after successful booking enquiry.</p>
				<hr>
				<h3><?php echo __( 'How to display form?' ); ?></h3>
				<p>For insert the Table booking form, add these shortcodes <code>[dmntb_booking style="horizontal"]</code> or <code>[dmntb_booking style="vertical"]</code> in your page to display Horizontal or vertical form layout respectively.</p>
			<?php endif; ?>
	</div>
</div>